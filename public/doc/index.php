<!DOCTYPE html>
  <head>
    <meta charset="utf-8" />
    <title>Documentación de API</title>
  </head>
  <body>
    <a href="../">Regresar</a>
    <hr />
    <div class="inner">
  <div class="header-meta"></div>
    <div> <h3 style="text-align: center"></a>Censos Economicos 2009 </h3>
      <hr align="CENTER"  width="950" color="Red" noshade>
    </div>
    <h2>Documentacion de API</h2>
    <ul class="list-1">
     <h2> La API se basa en los principios de REST y expone los datos de los censos economicos 2009 </h2>
    </ul>
    <p>&nbsp;</p>
  </div>
    
    <p>
      La URL raiz es <a href="http://lujanmartinezsarai.sun.fire/api/">http://lujanmartinezsarai.sun.fire/api/</a>.
    </p>
    <hr />

    <h2>Obtener XML correspondiente a los Datos del Censo</h2>

     Paginar los datos del censo:
Obtenga un archivo XML con una lista paginada de los datos del censo.<br>
URL: https://lujanmartinezsarai.sun.fire/censos/xml (url muestra)<br> 
*Metodo HTTP: GET<br> 
*Paginar los datos del censo por id_indicador<br>
*Obtenga un archivo XML con una lista paginada de los datos del censo.<br> 
URL: https://lujanmartinezsarai.sun.fire/censos/xml/id (url muestra)<br> 
Metodo HTTP: GET  clave: id_indicador (se requiere).
   </div>
    <ul>
      <li><strong>Parámetros</strong>:
      <ul>
        <li>id_indicador</li>
      </ul>
      </li>
      <li><strong>Presentacián de respuesta</strong>: XML</li>
      <li><strong>Ejemplo de respuesta</strong>:
<pre>
<img src="a1.jpg" alt="">
</pre>
      </li>
    </ul>

    <h2>Obtener XML correspondiente al tema</h2>
    Obtenga un archivo XML con una lista paginada de los temas.<br>
URL: https://lujanmartinezsarai.sun.fire/tema/xml/id_tema<br>
*Metodo HTTP: GET<br>
*Paginar los datos del tema<br>
*Obtenga un archivo XML con una lista paginada de los datos del tema.<br>
URL: https://lujanmartinezsarai.sun.fire/tema/xml/id_tema<br>
Metodo HTTP: GET  clave: id_tema (se requiere).
   </div>
    <ul>
      <li><strong>Parámetros</strong>:
      <ul>
        <li>id_tema</li>
      </ul>
      </li>
      <li><strong>Presentacián de respuesta</strong>: XML</li>
      <li><strong>Ejemplo de respuesta</strong>:
<pre>
<img src="a2.jpg" alt="">
</pre>
      </li>
    </ul>
<h2>Obtener XML correspondiente al tema sin id_tema</h2>
    Obtenga un archivo XML con una lista paginada de los temas sin el id_tema.<br>
URL: https://lujanmartinezsarai.sun.fire/tema/xml/<br>
*Metodo HTTP: GET<br>
*Paginar los datos del tema<br>
*Obtenga un archivo XML con una lista paginada de los datos del tema sin id_tema.<br>
URL: https://lujanmartinezsarai.sun.fire/tema/xml/<br>
Metodo HTTP: GET
   </div>
    <ul>
      <ul>
    
<li><strong>Presentacián de respuesta</strong: XML</lli>
      <li><strong>Ejemplo de respuesta</strong>:
<pre>
<img src="a3.jpg" alt="">

</pre>
      </li>
    </ul>
 <h2>Obtener XML correspondiente a los Indicadores de los censos</h2>

    Paginar los datos de los indicadores:
Obtenga un archivo XML con una lista paginada de los indicadores correspondientes a los censos.<br>
URL: https://lujanmartinezsarai.sun.fire/indicador/xml (url muestra)<br> 
*Metodo HTTP: GET<br> 
*Paginar los datos de los indicadores<br>
*Obtenga un archivo XML con una lista paginada de los indicadores correspondientes a los censos.<br> 
<li><strong>Presentaci�n de respuesta</strong>: XML</li>
      <li><strong>Ejemplo de respuesta</strong>:
    
    
    <pre>
<img src="i.jpg" alt="">
</pre>
    
     </li>

URL: https://lujanmartinezsarai.sun.fire/indicador/xml/id (url muestra)<br> 
Metodo HTTP: GET  clave: id_indicador (se requiere).
   </div>
    <ul>
      <li><strong>Par�metros</strong>:
      <ul>
        <li>id_indicador</li>
      </ul>
      </li>
      <li><strong>Presentaci�n de respuesta</strong>: XML</li>
      <li><strong>Ejemplo de respuesta</strong>:
    <img src="i1.jpg" alt="">
     </li>
    </ul>
<h2>Obtener XML correspondiente a las Entidades de los censos</h2>

    Paginar los datos de las entidades:
Obtenga un archivo XML con una lista paginada de las entidades correspondientes a los censos.<br>
URL: https://lujanmartinezsarai.sun.fire/entidad/xml (url muestra)<br> 
*Metodo HTTP: GET<br> 
*Paginar los datos de las entidades<br>
*Obtenga un archivo XML con una lista paginada de las entidades correspondientes a los censos.<br> 
<li><strong>Presentaci�n de respuesta</strong>: XML</li>
      <li><strong>Ejemplo de respuesta</strong>:
    <img src="e.jpg" alt="">
     </li>
URL: https://lujanmartinezsarai.sun.fire/entidad/xml/id (url muestra)<br> 
Metodo HTTP: GET  clave: id_entidad (se requiere).
   </div>
    <ul>
      <li><strong>Par�metros</strong>:
      <ul>
        <li>id_entidad</li>
      </ul>
      </li>
      <li><strong>Presentaci�n de respuesta</strong>: XML</li>
      <li><strong>Ejemplo de respuesta</strong>:
    <img src="e1.jpg" alt="">
     </li>
    </ul>
<h2>Modificar datos correspondientes a los indicadores</h2>
    Modificar descripcion de los indicadores:
URL: https://lujanmartinezsarai.sun.fire/indicador/id (url muestra)<br> 
Metodo HTTP: POST  clave: id_indicador (se requiere).
   </div>
    <ul>
      <li><strong>Par�metros</strong>:
      <ul>
        <li>id_indicador</li>
      </ul>
      </li>
      <li><strong>Presentaci�n de respuesta despueses de utilizar metodo GET con el id del indicador</strong>: XML</li>
      <li><strong>Ejemplo de respuesta</strong>:
    <img src="i2.jpg" alt="">
     </li>
    </ul>
<h2>Modificar datos correspondientes a una Entidades</h2>
     Modificar descripcion de Entidades:
URL: https://lujanmartinezsarai.sun.fire/entidad/id (url muestra)<br> 
Metodo HTTP: POST  clave: id_entidad (se requiere).
   </div>
    <ul>
      <li><strong>Par�metros</strong>:
      <ul>
        <li>id_entidad</li>
      </ul>
      </li>
      <li><strong>Presentaci�n de respuesta despues de utilizar metodo GET con el id de la entidad</strong>: XML</li>
      <li><strong>Ejemplo de respuesta</strong>:
    <img src="e2.jpg" alt="">
     </li>
    </ul>
<h2>Eliminar datos correspondientes a las Entidades de los censos</h2>
    Eliminar los datos de entidades:
Elimine los datos de una entidad que corresponda a un id.<br>
URL: https://lujanmartinezsarai.sun.fire/entidad/id (url muestra)<br> 
Metodo HTTP: DELETE clave: id_entidad (se requiere).
   </div>
    <ul>
      <li><strong>Par�metros</strong>:
      <ul>
        <li>id_entidad</li>
      </ul>
      </li>
      <li><strong>Presentaci�n de respuesta</strong>: XML</li>
      

    </ul>
<h2>Eliminar datos correspondientes a los indicadores de los censos</h2>
    Eliminar los datos de los indicadores:
Elimine los datos de un indicador que corresponda a un id.<br>
URL: https://lujanmartinezsarai.sun.fire/indicador/id (url muestra)<br> 
Metodo HTTP: DELETE clave: id_indicador (se requiere).
   </div>
    <ul>
      <li><strong>Par�metros</strong>:
      <ul>
        <li>id_indicador</li>
      </ul>
      </li>
      <li><strong>Presentaci�n de respuesta</strong>: XML</li>
      <li><strong>Ejemplo de respuesta</strong>:
    <img src="i3.jpg" alt="">
     </li>
    </ul>

      
  </body>
</html>
